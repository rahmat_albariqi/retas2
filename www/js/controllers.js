angular.module('rangrang.controllers', [])

.controller('AuthCtrl', function($scope, $location, Auth) {

  $scope.auth = Auth;
  $scope.auth.$onAuth(function(authData) {
    $scope.authData = authData;
  });
  $scope.loginEmail = {
    value : '',
  };
  $scope.loginPassword = {
    value : '',
  };
  $scope.loginSubmit = function(){
    console.log($scope.loginEmail.value);
    Auth.$authWithPassword( {
      email : $scope.loginEmail.value,
      password : $scope.loginPassword.value
    }).then(function(){
      $location.path('/tab/trending');
    }).catch(function(error){
      console.log(error);
      $scope.error = true;
      $scope.errorMessage = error;
    });
  }
})

.controller('TrendingCtrl', function($scope, $firebaseArray, $firebaseObject, query) {
  query().$loaded().then(function(){
    $scope.posts = query();
  })

  $scope.inputVote = function(postID){
    var postsRef = new Firebase("https://retas2.firebaseio.com/posts");
    var likeRef = postsRef.child(postID).child('vote');
    var like = $firebaseObject(likeRef); 
    like.$loaded().then(function(){
      if(like.count > 0){
        var myCount = like.count + 1;
        console.log(myCount);
        like.count = myCount
      }
      else{
        like.count = 1
      }

      like.$save().then(function(){
        console.log('ntaps');
      });

    })
  }

})

.controller('TrendingDetailCtrl', function($scope, $firebaseObject, $firebaseArray, $stateParams) {
  var usersRef = new Firebase("https://retas2.firebaseio.com/users");
  var postsRef = new Firebase("https://retas2.firebaseio.com/posts");
  var postID = postsRef.child($stateParams.postID);
  var post = $firebaseObject(postID);
  $scope.owner = false;
  $scope.volunteerJoined = false;
  post.$loaded().then(function(){
    if(post.category == 'event'){

      if(post.volunteer.enable == true){
        $scope.volunteerEnable = true;
        if(post.author.uid == $scope.authData.uid){
          $scope.owner = true;  
          $scope.volunteerJoined = true;
        }
        else{
          var lists = $firebaseArray(postID.child("volunteer").child("lists"));
          console.log('loop');
          lists.$loaded().then(function(){
            console.log(lists);
            angular.forEach(lists, function(userid){
              console.log(userid.uid);
              console.log($scope.authData.uid);
              $scope.volunteerJoined = (userid.uid == $scope.authData.uid) ? true : false;
            })
          })
        }
      }
      if(post.donate.enable == true){
        $scope.donateEnable = true;
      }
    }
    $scope.post = post;
  })

  $scope.inputVolunteer = function(){
    var volunteer = $firebaseArray(postID.child('volunteer').child('lists'));
    volunteer.$add({
      uid : $scope.authData.uid
    }).then(function(){
      $scope.volunteerJoined = true;
    }).then(function(){
      var activityLogsRef = usersRef.child($scope.authData.uid).child('activityLogs');
      var activityLogs = $firebaseArray(activityLogsRef);
      console.log(post);
      console.log(post.title);
      activityLogs.$add({
        type : 'joinVolunteer',
        postID : post.$id,
        postTitle : post.title,
        timestamp : Firebase.ServerValue.TIMESTAMP
      })
    })
  }
})

.controller('PostCtrl', function($scope, $location, $firebaseArray) {
  $scope.inputTitle = {
    value : ''
  };
  $scope.inputContent = {
    value : ''
  };
  $scope.postSubmit = function(){
    var postsRef = new Firebase("https://retas2.firebaseio.com/posts");
    var posts = $firebaseArray(postsRef);
    var date = new Date();
    console.log($scope.inputContent.value);
    posts.$add({
      title : $scope.inputTitle.value,
      content : $scope.inputContent.value,
      /*category : $scope.inputCategory.value,
      author : {
        uid : $scope.authData.uid,
        firstname : authorName.firstname,
        lastname : authorName.lastname
      },*/
      created : date,
      timestamp : Firebase.ServerValue.TIMESTAMP
      }).then(function(){
        console.log("Success");
        $location.path('/');
      })
    
  }
})

.controller('EventCtrl', function($scope, $location, $firebaseArray, $firebaseObject, $ionicHistory) {
  $scope.inputTitle = {
    value : ''
  };
  $scope.inputContent = {
    value : ''
  };
  $scope.inputVolunteer = {
    value : 'false'
  };
  $scope.inputDonate = {
    value : 'false'
  };

  $scope.inputDateStart = {
    value : ''
  }
  
  $scope.inputDateEnd = {
    value : ''
  }

  $scope.postEventSubmit = function(){
    var postsRef = new Firebase("https://retas2.firebaseio.com/posts");
    var posts = $firebaseArray(postsRef);

    var usersRef = new Firebase("https://retas2.firebaseio.com/users");
    var user = $firebaseObject(usersRef.child($scope.authData.uid));
    if($scope.inputVolunteer.value == true){
      $scope.inputVolunteer.value = true;
    }
    if($scope.inputDonate.value == true){
      $scope.inputDonate.value = true;
    }

    user.$loaded().then(function(){
      var dateStart = angular.toJson($scope.inputDateStart.value)
      var dateEnd = angular.toJson($scope.inputDateEnd.value)
      var newDate = dateStart.slice(1,11);
      var endDate = dateEnd.slice(1,11);
        posts.$add({
        title : $scope.inputTitle.value,
        content : $scope.inputContent.value,
        category : 'event',
        volunteer : {
          enable : $scope.inputVolunteer.value
        },
        donate : {
          enable : $scope.inputDonate.value
        },
        author : {
          uid : $scope.authData.uid,
          firstname : user.firstname,
          lastname : user.lastname
        },
        eventStart : newDate,
        eventEnd : endDate,
        timestamp : Firebase.ServerValue.TIMESTAMP
      }).then(function(postsRef){
        var activityLogsRef = usersRef.child($scope.authData.uid).child('activityLogs');
        var activityLogs = $firebaseArray(activityLogsRef);
        activityLogs.$add({
          type : 'createEvent',
          postID : postsRef.key(),
          postTitle : $scope.inputTitle.value,
          timestamp : Firebase.ServerValue.TIMESTAMP
        })
          $scope.inputTitle = "";
          $scope.inputContent = "";
          $scope.inputVolunteer = false;
          $scope.inputDonate = false;
          console.log("Success");
          $location.path('/');
          $ionicHistory.clearHistory();
      })
    })    
  }
})

.controller('DiscussCtrl', function($scope, $location, $firebaseArray, $firebaseObject, $ionicHistory) {
  $scope.inputTitle = {
    value : ''
  };
  $scope.inputContent = {
    value : ''
  };
  
  $scope.postDiscussSubmit = function(){
    var postsRef = new Firebase("https://retas2.firebaseio.com/posts");
    var posts = $firebaseArray(postsRef);
    var date = new Date();

    var usersRef = new Firebase("https://retas2.firebaseio.com/users");
    var user = $firebaseObject(usersRef.child($scope.authData.uid));

    user.$loaded().then(function(){
        posts.$add({
        title : $scope.inputTitle.value,
        content : $scope.inputContent.value,
        category : 'discuss',
        author : {
          uid : $scope.authData.uid,
          firstname : user.firstname,
          lastname : user.lastname
        },
        timestamp : Firebase.ServerValue.TIMESTAMP
      }).then(function(postsRef){
        var activityLogsRef = usersRef.child($scope.authData.uid).child('activityLogs');
        var activityLogs = $firebaseArray(activityLogsRef);
        
        activityLogs.$add({
          type : 'createDiscuss',
          postID : postsRef.key(),
          postTitle : $scope.inputTitle.value,
          timestamp : Firebase.ServerValue.TIMESTAMP
        })
          $scope.inputTitle = "";
          $scope.inputContent = "";
          console.log("Success");
          $location.path('/');
          $ionicHistory.clearHistory();
      })
    })    
  }
})

.controller('AccountCtrl', function($scope, $firebaseObject, $firebaseArray, $stateParams) {
  var usersRef = new Firebase("https://retas2.firebaseio.com/users");
  var user = usersRef.child($stateParams.userID);
  $scope.userID = $stateParams.userID;
  $scope.account = $firebaseObject(user);
  $scope.activityLogs = $firebaseArray(user.child('activityLogs'));
})

.controller('AccountEditCtrl', function($scope, $firebaseObject) {
  var usersRef = new Firebase("https://retas2.firebaseio.com/users");
  var user = usersRef.child($scope.authData.uid);
  $firebaseObject(user).$loaded().then(function(){
    $scope.profile = $firebaseObject(user);
  })
  
})

.controller('AccountLevelCtrl', function($scope, $firebaseObject) {
  $scope.goodguy = false;
  $scope.heroic = false;
  $scope.superhero = false;
  var usersRef = new Firebase("https://retas2.firebaseio.com/users");
  var user = usersRef.child($scope.authData.uid);
  $firebaseObject(user).$loaded().then(function(){
    $scope.profile = $firebaseObject(user);
    switch($scope.profile.volunteer){
      case 10:
        $scope.goodguy = true;
        break;
      case 25:
        $scope.heroic = true;
        break;
      case 50:
        $scope.superhero = true;
        break;
    }
  })
});
