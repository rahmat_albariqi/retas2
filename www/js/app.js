// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
angular.module('rangrang', ['ionic', 'rangrang.controllers', 'rangrang.services', 'firebase'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider, $ionicConfigProvider) {
  $ionicConfigProvider.tabs.position('bottom');
  $ionicConfigProvider.views.transition('ios');

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider

  // setup an abstract state for the tabs directive
    .state('tab', {
    url: '/tab',
    abstract: true,
    templateUrl: 'templates/tabs.html'
  })

  // Each tab has its own nav history stack:

  .state('tab.trending', {
    url: '/trending',
    views: {
      'tab-trending': {
          templateUrl: 'templates/tab-trending.html',
          controller: 'TrendingCtrl'
        }
      }
    })
  .state('tab.trending-detail', {
    url: '/trending/:postID',
    views: {
      'tab-trending': {
        templateUrl : 'templates/trending-detail.html',
        controller : 'TrendingDetailCtrl'
      }
    }
  })
  .state('tab.trending-account', {
        url: '/trending/account/:userID',
        views: {
          'tab-trending': {
            templateUrl: 'templates/tab-account.html',
            controller: 'AccountCtrl'
          }
        }
      })

  .state('tab.post', {
      url: '/post',
      views: {
        'tab-post': {
          templateUrl: 'templates/tab-post.html',
          controller: 'PostCtrl'
        }
      }
    })
    .state('tab.post-event', {
      url: '/post/event',
      views: {
        'tab-post': {
          templateUrl : 'templates/post-event.html',
          controller : 'EventCtrl'
        }
      }
    })
    .state('tab.post-discuss', {
      url: '/post/discuss',
      views: {
        'tab-post': {
          templateUrl : 'templates/post-discuss.html',
          controller : 'DiscussCtrl'
        }
      }
    })

  .state('tab.account', {
    url: '/account/:userID',
    views: {
      'tab-account': {
        templateUrl: 'templates/tab-account.html',
        controller: 'AccountCtrl'
      }
    }
  })
  .state('tab.account-edit', {
      url: '/account/:userID/edit',
      views: {
        'tab-account': {
          templateUrl : 'templates/account-edit.html',
          controller : 'AccountEditCtrl'
        }
      }
    })
  .state('tab.account-level', {
      url: '/account/:userID/level',
      views: {
        'tab-account': {
          templateUrl : 'templates/account-level.html',
          controller : 'AccountLevelCtrl'
        }
      }
    })
  .state('tab.account-logs', {
      url: '/account/logsdetail/:postID',
      views: {
        'tab-account': {
          templateUrl : 'templates/trending-detail.html',
          controller : 'TrendingDetailCtrl'
        }
      }
    })


  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/tab/trending');

});
