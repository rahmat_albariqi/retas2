angular.module('rangrang.services', [])

.factory('loginName', function ($firebaseObject){
  var usersRef = new Firebase("https://retas2.firebaseio.com/users");
  return {
    firstname : function(uid){
      var firstname = $firebaseObject(usersRef.child(uid).child('firstname'));
      return firstname.$value;
    },

    lastname : function(uid){
      var lastname = $firebaseObject(usersRef.child(uid).child('lastname'));  
      console.log('oi');
      return lastname;
    }
  }
})

.factory('query', function ($firebaseArray) {
  var postsRef = new Firebase("https://retas2.firebaseio.com/posts");
  var array = $firebaseArray(postsRef)
  return function(q){
    if ( q == undefined ){
      return array;
    }
  }
})

.factory('Auth', function ($firebaseAuth) {
  var ref = new Firebase("https://retas2.firebaseio.com/");
  return $firebaseAuth(ref);
});