angular.module('starter.controllers', [])

.controller('AuthCtrl', function($scope, $location, Auth) {
  $scope.auth = Auth;
  $scope.auth.$onAuth(function(authData) {
    console.log("auth check");
    $scope.authData = authData;
  });
  
  $scope.loginFacebook = function(){
    console.log("fuck u");
    Auth.$authWithOAuthRedirect("facebook").then(function(authData){
      console.log("success")
      window.location = '/tab/dash'
    })
    .catch(function(error){
      console.log(error)
    })
  }

  $scope.loginSubmit = function(){
    Auth.$authWithPassword( {
      email : $scope.loginEmail,
      password : $scope.loginPassword
    }, function(error, authData) {
    if(error) {
      console.log("anjau");
      }
    }).then(function(){
      $location.path('/tab/dash');
    });
  }
})

.controller('DashCtrl', function($scope, $firebaseArray) {
  var postsRef = new Firebase("https://retas2.firebaseio.com/posts");
  var array = $firebaseArray(postsRef)
  array.$loaded().then(function(){
    $scope.posts = array;
  })
})

.controller('ChatsCtrl', function($scope, $location, $firebaseArray) {
  $scope.inputTitle = {
    value : ''
  };
  $scope.inputContent = {
    value : ''
  };
  $scope.postSubmit = function(){
    var postsRef = new Firebase("https://retas2.firebaseio.com/posts");
    var posts = $firebaseArray(postsRef);
    var date = new Date();
    console.log($scope.inputContent.value);
    posts.$add({
      title : $scope.inputTitle.value,
      content : $scope.inputContent.value,
      /*category : $scope.inputCategory.value,
      author : {
        uid : $scope.authData.uid,
        firstname : authorName.firstname,
        lastname : authorName.lastname
      },*/
      created : date,
      timestamp : Firebase.ServerValue.TIMESTAMP
      }).then(function(){
        console.log("Success");
        $location.path('/');
      })
    
  }
})

.controller('ChatDetailCtrl', function($scope, $stateParams, Chats) {
  $scope.chat = Chats.get($stateParams.chatId);
})

.controller('AccountCtrl', function($scope) {
  $scope.settings = {
    enableFriends: true
  };
});
